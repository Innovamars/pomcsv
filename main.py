#Dependencias 
import csv
from flask import Flask, render_template

#Configuracion del app
app = Flask(__name__)
app.config['DEBUG'] = True

#Redirecionamientos
@app.route("/")
def index():
    return "hola"

@app.route("import")
def importcsv():
    with open('name.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            return print(row['Partida'], row['Monto'])

if __name__ == "__main__":
    app.run()